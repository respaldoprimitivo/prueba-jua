# Prueba JUA&

### Requerimiento principal

Desarrollar un tema en wordpress basado en las imágenes que se encuentran en el directorio **"referentes"**.

Debe quedar lo más parecido posible utilizando recursos random y en los textos se puede usar un lorem ipsum generator online.

En la landing principal hay dos espacios con sliders, por lo que deben colocarlos. Y en el primer slider uno de los banners debe estar enlazado a la interna.

### Condiciones

- Para ser evaluados se debe realizar un Fork de este repositorio y luego subir la carpeta del theme que se desarrollará en wordpress. Compartir fork con usuario de gitlab **@techjua**
- No se puede usar un plugin de constructor visual, solo las herramientas que nos dá wordpress nativamente o scripts externos. Se evaluará el CSS aplicado.
- Se pueden desarrollar plugins para realizar algunas tareas.
- El sitio debe ser administrable con facilidad.
- Los banners deben tener una sección independiente para administrarlos desde el panel de wordpress.
- El sitio web debe ser responsive design.
- El resultado final debe alojarse en algún enlace accesible desde internet (si se presentan inconvenientes con esta condición se debe notificar con anticipación).
- Entrega del proyecto en el tiempo pactado.

### Adicional a todas las condiciones expuestas anteriormente, puedes proponer todas tus ideas, se evaluará la creatividad y selección de técnicas que elijas para solucionar esta prueba

# Se te desea mucho éxito.